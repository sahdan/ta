#!/usr/bin/env python

import os
import sys

lib_path = os.path.realpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'app', 'lib'))
if lib_path not in sys.path:
    sys.path[0:0] = [lib_path]

import customutils
import preparators
import simplejson as json
import math
import os
import argparse
import numpy as np
from sklearn.feature_extraction import FeatureHasher
from sklearn import preprocessing, model_selection
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
import collections
import random
import time
import psutil
import pickle
from tensorflow.keras.models import Sequential, load_model, model_from_json
from tensorflow.keras.layers import Dense
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from matplotlib import pyplot

def classify(extracted_data, urls):

    NUM_FEATURES = 188

    # urls = [line.rstrip('\n') for line in open("app/static/urls.txt")]
    pages = []

    # load the json data associated with the urls
    for i in range(len(urls)):
        # data = {}
        data = extracted_data[i]
        # id = str(i).zfill(get_total_digit(len(urls)))
        # with open('app/static/%s.json' % id) as f:
        #     data = json.load(f)

        # process data
        preparator = preparators.Preparators(data)

        # release data from memory
        del data
        # prepare features
        discrete_features= preparator.prepare()

        # Continue only if there are texts on the data
        # Else, the program likely unable to load and extract the page
        if (len(preparator.texts)) == 0:
            pages.append("Unable to load this page")
        else:
            vectorizer = FeatureHasher(n_features=NUM_FEATURES)
            discrete_features = vectorizer.fit_transform(discrete_features).toarray()

            # Set np print output to max so it shows all data.
            np.set_printoptions(threshold=sys.maxsize)


            # np.savetxt("train_x.csv", train_x, delimiter=",")

            # load the NN model from a file and test the dataset
            labels = test_nn(discrete_features)

            golden_texts = []

            # Harvest the golden text and mark them up according to the label
            for text, label in zip(preparator.texts, labels):
                if label != 0:
                    html_text = ""
                    if label == 1:
                        html_text = "<h1>" + "".join(text["text"]) + "</h1>"
                    elif label == 2:
                        html_text = "<p><em>" + "".join(text["text"]) + "</em></p>"
                    else:
                        html_text = "<p>" + "".join(text["text"]) + "</p>"

                    golden_texts.append(html_text)

            if len(golden_texts) == 0:
                golden_texts.append("This page has no content")
            pages.append("".join(golden_texts))

    return pages

def test_nn(test_x):
    # load json and create model
    model = load_nn_model()

    # Defining the optimizer and loss function
    model.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics=['accuracy'])

    # print('Neural Network Model Summary: ')
    # print(model.summary())

    # Predict using the model
    pred_y = model.predict(test_x)

    labels = []

    for i in range(len(pred_y)):
        for j in range(4):
            # Convert softmax output values to either one or zero
            pred_y[i][j] = 1 if np.argmax(pred_y[i]) == j else 0

            # Convert [x x x x] into labels ranging from 0-3
            if pred_y[i][j] == 1:
                labels.append(j)

    # Change the values data type from float to int
    # pred_y = pred_y.astype(int)

    return labels


def load_nn_model():
    json_file = open('model.json', 'r')
    model_json = json_file.read()
    json_file.close()
    model = model_from_json(model_json)
    # load weights into new model
    model.load_weights("model.h5")
    return model

def get_total_digit(num):
    return int(math.log10(num))+1

if __name__ == '__main__':
    classify()
