This model is trained using the 5 websites.
Features: Tag path dan CSS Properties
Output: [non-content,title,author,content]
Hidden layers: 2, 96 nodes and 20 nodes respectively
Patience: 10 epoch
Loss Function: Categorial Crossentrophy
Class Weights: [ 0.28033877 61.06712963 39.02514793  2.55828161]
Dropout on 2 hidden layers with value of 0.5
