# Testing python's object comparison in array
# arr = [
# {"name": "Tom", "age": 10},
# {"name": "Mark", "age": 5},
# {"name": "Pam", "age": 7}
# ]

# person = {"name": "Tom", "age": 10}

# if person in arr:
#     print("Tom is in arr")
# else:
#     print("Tom is not in arr")

import simplejson as json
import numpy as np
import mlpy
# Calculating tallies of labeling
# for id in range(10):
#     id = str(id).zfill(2)
#     with open('app/data/usatoday/%s.json' % id) as f:
#         data = json.load(f)

#     total_texts = len(data['texts'])
#     labels = [0,0,0,0]
#     labels_str = []
#     labels_err = ""


#     for text in data['texts']:
#         if text['label'] == 0:
#             labels[0] = labels[0] + 1
#         elif text['label'] == 1:
#             labels[1] = labels[1] + 1
#         elif text['label'] == 2:
#             labels[2] = labels[2] + 1
#         elif text['label'] == 3:
#             labels[3] = labels[3] + 1
#         else:
#             labels_err = labels_err + " " + text['label']

#     for label in labels:
#         labels_str.append(str(label))
#     print(",".join(labels_str) + "," + str(total_texts) + "," + labels_err)


# Auto text labeling for a certain json file using index
# with open('app/data/tipspintar/38.json') as f:
#     data = json.load(f)

# for idx, text in enumerate(data['texts']):
#     if idx > 49 and idx < 293:
#         text['label'] = 3

# with open('app/data/tipspintar/38.json', 'w') as f:
#     f.write(json.dumps(data, indent=2, ensure_ascii=False))

# Labelling json file according to nn prediction
# for id in range(1):
#     id = str(id).zfill(2)
#     with open('app/static/%s.json' % id) as f:
#         data = json.load(f)
#     pred_y = np.genfromtxt('pred_y.csv', delimiter=",")

#     for i in range(len(pred_y)):
#         data["texts"][i]["label"] = int(pred_y[i])

#     with open('app/static/%s.json' % id, 'w') as f:
#         f.write(json.dumps(data, indent=2, ensure_ascii=False))

# Label Texts with 0s as default
# for id in range(20):
#     id = str(id).zfill(2)
#     with open('app/data/usatoday/%s.json' % id) as f:
#         data = json.load(f)

#     for text in data['texts']:
#         text['label'] = 0

#     with open('app/data/usatoday/%s.json' % id, 'w') as f:
#         f.write(json.dumps(data, indent=2, ensure_ascii=False))

# Testing about clustering feature extraction
# computed_text= {
#         "background-color": "rgb(34, 34, 34)",
#         "border-bottom-color": "rgb(255, 255, 255)",
#         "border-left-color": "rgb(255, 255, 255)",
#         "border-right-color": "rgb(255, 255, 255)",
#         "border-top-color": "rgb(255, 255, 255)",
#         "color": "rgb(255, 255, 255)",
#         "column-rule-color": "rgb(255, 255, 255)",
#         "font-family": "montserrat, helvetica, arial, sans-serif",
#         "font-size": "14px",
#         "font-weight": "bold",
#         "line-height": "19px",
#         "list-style-type": "none",
#         "outline-color": "rgb(255, 255, 255)",
#         "padding-bottom": "10px",
#         "padding-left": "16px",
#         "padding-right": "16px",
#         "padding-top": "10px",
#         "perspective-origin": "38.21875px 19.5px",
#         "text-align": "left",
#         "text-transform": "uppercase",
#         "transform-origin": "38.21875px 19.5px",
#         "transition-duration": "0.2s",
#         "transition-timing-function": "linear"
# }

# fitur = dict(list(computed_text.items()))
# print(fitur)

# Testing lcs algorithm from mlpy
a = [1,2,3,4,5,6,7]
b = [1,2,3,3,6,6,7]
a = np.array(list(a), dtype='U1').view(np.uint32)
b = np.array(list(b), dtype='U1').view(np.uint32)
length, path = mlpy.lcs_std(a, b)
print(length) # 5
