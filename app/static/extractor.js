var fs, page, system;

system = require('system');

fs = require('fs');

page = require('webpage').create();

// set a big enough viewport size,
// this needs to be static
page.viewportSize = {
  width: 1600,
  height: 4000
};

// debug info
page.onResourceRequested = function(request) {
  system.stderr.write(JSON.stringify(request, void 0, 2));
  return system.stderr.write('\n\n');
};

// debug info
page.onResourceReceived = function(response) {
  system.stderr.write(JSON.stringify(response, void 0, 2));
  return system.stderr.write('\n\n');
};

// debug info
page.onConsoleMessage = function(message) {
  system.stderr.write(message);
  return system.stderr.write('\n\n');
};

// handle page loadd
page.onLoadFinished = function(status) {
  var data;
  if (status === !'success') {
    // bail on network issue
    return phantom.exit();
  }
  // inject javascripts helpers needed
  page.evaluate(function() {
    String.prototype.trim = function() {
      return this.replace(/^\s+|\s+$/g, '');
    };
    // namespace creation
    (function(window) {
      'use strict';
      var namespace;
      window.__slice = [].slice;
      namespace = function(target, name, block) {
        var i, item, len, ref, top;
        if (arguments.length < 3) {
          [target, name, block] = [window, ...arguments];
        }
        top = target;
        ref = name.split('.');
        for (i = 0, len = ref.length; i < len; i++) {
          item = ref[i];
          target = target[item] || (target[item] = {});
        }
        return block(target, top);
      };
      return namespace('spider', function(exports, top) {
        return exports.namespace = namespace;
      });
    })(window);
    // spider.utils
    return spider.namespace('spider.utils', function(exports) {
      'use strict';
      exports.is_valid = function(value) {
        var re;
        re = /^[a-zA-Z][a-zA-Z0-9\-_]+$/;
        return value && re.test(value);
      };
      // get element description
      exports.element = function(element, is_name_only) {
        var c, classes, data, i, len, name, ref;
        name = element.tagName.toLowerCase();
        if (is_name_only) {
          return name;
        }
        classes = [];
        ref = element.classList;
        for (i = 0, len = ref.length; i < len; i++) {
          c = ref[i];
          if (exports.is_valid(c)) {
            classes.push(c);
          }
        }
        data = {
          name: name,
          id: exports.is_valid(element.id) ? element.id : '',
          classes: classes.sort()
        };
        return data;
      };
      // generate tag path
      exports.path = function(element, is_name_only) {
        var path;
        path = [];
        while (element) {
          if (element === document.body) {
            break;
          }
          path.splice(0, 0, exports.element(element, is_name_only));
          element = element.parentElement;
        }
        return path;
      };
      // calculate block bound
      exports.bound = function(element) {
        var bound, rect, scrollLeft, scrollTop;
        scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        scrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft;
        rect = element.getBoundingClientRect();
        bound = {
          width: rect.width,
          height: rect.height,
          left: rect.left + scrollLeft,
          top: rect.top + scrollTop
        };
        return bound;
      };
      // calculate computed css
      return exports.computed = function(element) {
        var computed, data, defaults, i, key, len;
        defaults = document.defaultView.getComputedStyle(document.body);
        computed = document.defaultView.getComputedStyle(element);
        data = {};
        for (i = 0, len = computed.length; i < len; i++) {
          key = computed[i];
          if (key === 'width' || key === 'height' || key === 'top' || key === 'left' || key === 'right' || key === 'bottom') {
            // don't care about dimension, let bound track that
            continue;
          }
          if (key.charAt(0) === '-') {
            // don't care about webkit specific
            continue;
          }
          if (computed[key] === defaults[key]) {
            // don't care about default value
            continue;
          }
          data[key] = computed[key];
        }
        return data;
      };
    });
  });
  // extract page basic data
  data = page.evaluate(function() {
    var computed, descriptions, i, key, len, meta, ref, title, titles;

    // find title and description
    titles = (function() {
      var i, len, ref, results;
      ref = document.querySelectorAll('title');
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        title = ref[i];
        results.push(title.innerText);
      }
      return results;
    })();
    descriptions = (function() {
      var i, len, ref, results;
      ref = document.querySelectorAll('meta[name="description"]');
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        meta = ref[i];
        results.push(meta.content);
      }
      return results;
    })();

    // open graph title and description
    titles.push(...((function() {
      var i, len, ref, results;
      ref = document.querySelectorAll('meta[name="og:title"], meta[property="og:title"]');
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        meta = ref[i];
        results.push(meta.content);
      }
      return results;
    })()));
    descriptions.push(...((function() {
      var i, len, ref, results;
      ref = document.querySelectorAll('meta[name="og:description"], meta[property="og:description"]');
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        meta = ref[i];
        results.push(meta.content);
      }
      return results;
    })()));

    // twitter title and description
    titles.push(...((function() {
      var i, len, ref, results;
      ref = document.querySelectorAll('meta[name="twitter:title"], meta[property="twitter:title"]');
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        meta = ref[i];
        results.push(meta.content);
      }
      return results;
    })()));
    descriptions.push(...((function() {
      var i, len, ref, results;
      ref = document.querySelectorAll('meta[name="twitter:description"], meta[property="twitter:description"]');
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        meta = ref[i];
        results.push(meta.content);
      }
      return results;
    })()));
    // computed style for body
    computed = {};
    ref = document.defaultView.getComputedStyle(document.body);
    for (i = 0, len = ref.length; i < len; i++) {
      key = ref[i];
      if (key.charAt(0) === '-') {
        // don't care about webkit specific
        continue;
      }
      // don't care about default value
      computed[key] = document.defaultView.getComputedStyle(document.body)[key];
    }
    data = {
      url: window.location.href,
      titles: titles,
      descriptions: descriptions,
      body: {
        scroll: {
          top: document.documentElement.scrollTop || document.body.scrollTop,
          left: document.documentElement.scrollLeft || document.body.scrollLeft
        },
        bound: spider.utils.bound(document.body),
        computed: computed
      }
    };
    return data;
  });
  // extract links
  data.links = page.evaluate(function() {
    var i, len, link, ref, results;
    ref = document.querySelectorAll('a[href]');
    results = [];
    for (i = 0, len = ref.length; i < len; i++) {
      link = ref[i];
      results.push(link.href);
    }
    return results;
  });
  // extract data
  data.texts = page.evaluate(function() {
    var bound, computed, node, text, texts, walker;
    texts = [];
    // walk over all text in the page
    walker = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, null, false);
    while (text = walker.nextNode()) {
      if (!(text.nodeValue.trim().length > 0)) {
        continue;
      }
      // container node
      node = text.parentElement;
      bound = spider.utils.bound(node);
      if (!(bound.width * bound.height > 0)) {
        continue;
      }
      // find the parent node that is a block
      while (node) {
        computed = document.defaultView.getComputedStyle(node);
        if (parseInt(computed.width) * parseInt(computed.height) > 0) {
          break;
        }
        node = node.parentElement;
      }
      if (!node) {
        continue;
      }
      // have we seen this node?
      if (node.spider) {
        node.spider.text.push(text.nodeValue);
        continue;
      }
      // collect features
      node.spider = {
        element: spider.utils.element(node),
        path: spider.utils.path(node, true),
        selector: spider.utils.path(node),
        text: [text.nodeValue],
        html: node.innerHTML,
        bound: spider.utils.bound(node),
        computed: spider.utils.computed(node)
      };
      texts.push(node.spider);
      // debug
      node.style.border = '1px solid red';
    }
    return texts;
  });
  // extract images
  data.images = page.evaluate(function() {
    var bound, i, images, len, node, ref;
    images = [];
    ref = document.querySelectorAll('img[src]');
    for (i = 0, len = ref.length; i < len; i++) {
      node = ref[i];
      bound = spider.utils.bound(node);
      if (!(bound.width * bound.height > 0)) {
        continue;
      }
      images.push({
        src: node.src,
        element: spider.utils.element(node),
        path: spider.utils.path(node, true),
        selector: spider.utils.path(node),
        bound: bound,
        computed: spider.utils.computed(node)
      });
    }
    return images;
  });
  // write json
  fs.write(system.args[2] + '.json', JSON.stringify(data, void 0, 2));
  page.render(system.args[2] + '.png');
  // done
  return phantom.exit();
};

// argument check
if (system.args.length !== 3) {
  system.stderr.write('Usage: phantomjs ' + system.args[0] + ' <url> <label>\n\n');
  phantom.exit();
}

// load page
page.open(system.args[1]);
