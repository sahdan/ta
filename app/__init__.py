from flask import Flask
app = Flask(__name__)
app.secret_key = "randomstring"
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['MAX_CONTENT_PATH'] = 1024000
from app import routes
