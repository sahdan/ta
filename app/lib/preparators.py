from sklearn.feature_extraction import DictVectorizer
import numpy as np
import tokenizers
import analyzers
import collections
import itertools
import customutils
import re
from sklearn import preprocessing

class Preparators(object):

    def __init__(self,data):
        self.texts = []

        if 'texts' in data:
            for text in data['texts']:
                self.texts.append(text)


    def prepare(self):
        """
        Prepare data for neural network classifier model.
        """

        # build features
        discrete_features = []

        for text in self.texts:

            # discrete features
            discrete_feature = dict()

            # CSS Properties features
            discrete_feature = dict(text['computed'].items())

            # Tag Path features
            discrete_feature['path'] = ' > '.join(text['path'])

            # CSS Selectors features
            discrete_feature['class'] = ' > '.join([
                '%s%s' % (
                    selector['name'],
                    '.' + '.'.join(selector['classes']) if selector['classes'] else '',
                )
                for selector in text['selector']
            ])

            discrete_features.append(discrete_feature)

        return discrete_features
