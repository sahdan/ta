# From app Package, import app variable (a Flask instance) defined in __init__.py
import json
import requests
import time
import os
import math
import secrets
import string

from urllib.parse import urlparse  # To Parse Domain name

import scrapy

from twisted.internet import reactor

from multiprocessing import Process

from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy import signals
from scrapy_splash import SplashRequest

from flask import render_template
from flask import request
from flask import make_response
from flask import session
from app import app

import classifier


class MyCrawler(CrawlSpider):
    name = 'MyCrawler'
    start_urls = ['https://www.detik.com/']
    max_page = 1        # This is just for declaration, the value will be overrode by the input parameter
    rules = (
        Rule(LinkExtractor(allow=()),
             callback='parse_item', follow=True),
    )

    custom_settings = {
        'DOWNLOAD_DELAY': 1,
        'CONCURRENT_REQUESTS_REQUESTS_PER_IP': 1,
        'ROBOTSTXT_OBEY': True,
        'COOKIES_ENABLED': False
    }

    nodes = []
    links = []
    urls = []

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(MyCrawler, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.quit, signals.spider_closed)
        return spider

    # Overriding constructor so it can take custom arguments
    def __init__(self, sid, url='https://www.detik.com/', max_page=10, domain_specific=False, *args, **kwargs):
        super(MyCrawler, self).__init__(*args, **kwargs)
        self.start_urls = [url]
        self.max_page = int(max_page)
        self.sid = sid

         # Parse Domain name of the start url
        parsed_uri = urlparse(url)

        if domain_specific:
            self.allowed_domains = ['{uri.netloc}'.format(uri=parsed_uri)]

        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        # The custom settings is set here because it won't work if it is set outside of this constructor

        # The code block below doesn't work at all.
        # self.rules = (
        #     Rule(LinkExtractor(allow_domains=('{uri.netloc}'.format(uri=parsed_uri))),
        #         callback='parse_item', follow=True),
        # )

        node = {
            'url': url,
            'domain': domain
        }

        self.nodes.append(node) # Add the start url to node
        self.urls.append(url) # Add the start url to urls

    def parse_item(self, response):
        # Parse Domain name
        parsed_uri = urlparse(response.url)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)

        node = {
            'url': response.url,
            'domain': domain
        }

        source_url = response.request.headers.get('Referer', 'no referrer').decode('utf-8')
        if node not in self.nodes and len(self.nodes) < self.max_page and source_url in self.urls:
            self.nodes.append(node)
            self.urls.append(response.url)

        if response.url in self.urls and source_url in self.urls:
            link = {
                'source': source_url,
                'target': response.url,
                'depth': response.meta['depth']
            }
            self.links.append(link)

    def quit(self, spider):
        # Since CLOSESPIDER_PAGECOUNT cannot be accurate, we need to manually slice the first N element from nodes
        # self.nodes = self.nodes[:self.max_page]
        # self.urls = self.urls[:self.max_page]

        for idx,node in enumerate(self.nodes):
            node['id'] = idx+1

        data = {

            'nodes': self.nodes,
            'links': self.links,
        }


        with open('app/static/nodes-' + self.sid + '.json', 'w') as fp:
            json.dump(data, fp, indent=4)

        with open('app/static/urls-'+ self.sid + '.txt', 'w') as f:
            for url in self.urls:
                f.write("%s\n" % url)




def crawl_web(par_url, par_depth, par_total, par_domain_specific, sid):
    configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})
    runner = CrawlerRunner({
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
        'DEPTH_LIMIT': int(par_depth),
        'CLOSESPIDER_PAGECOUNT': int(par_total)
    })

    d = runner.crawl(MyCrawler, sid=sid, url=par_url, max_page=par_total, domain_specific=par_domain_specific)
    d.addBoth(lambda _: reactor.stop())
    reactor.run()  # the script will block here until the crawling is finished

# Function to handle multi processing. To allow the spider to run the reactor multiple times
def run_crawler(par_url, par_depth, par_total, par_domain_specific, sid):
    p = Process(target=crawl_web, args=(par_url, par_depth, par_total,par_domain_specific, sid))
    p.start()
    p.join()

def get_total_digit(num):
    return int(math.log10(num))+1

def save_to_session(data, urls):
    session['nodes'] = data
    session['urls'] = urls

def extract_data_from_urls(urls):
    with open('script.lua', 'r') as file:
        script = file.read()
    urls_len = len(urls)
    extracted_data = []
    for i in range(urls_len):
        resp = requests.post('http://localhost:8050/run', json={
        'lua_source': script,
        'url': urls[i]
        })
        # Converting Bytes to String
        json_raw = resp.content.decode('utf-8')
        # Converting String to JSON/Dictionary
        json_data = json.loads(json_raw)

        extracted_data.append(json_data)
        # fileName = 'app/static/' + str(i).zfill(get_total_digit(urls_len)) + '.json'
        # with open(fileName, 'w') as fp:
        #     json.dump(json_data, fp, indent=4, sort_keys=True)
        time.sleep(1)

    return extracted_data

# Complete process form
@app.route('/')
@app.route('/index')
def index():
  return render_template('index.html')

# Result from complete process
@app.route('/result')
def result():
    par_url = request.args.get('url')
    par_depth = request.args.get('depth')
    par_total = request.args.get('total')
    par_domain_specific = request.args.get('specific')

    # Generate a session ID for file name identification
    sid_length = 10
    sid = ''.join(secrets.choice(string.ascii_uppercase + string.digits)
                                                  for i in range(sid_length))

    run_crawler(par_url, par_depth, par_total, par_domain_specific, sid)

    urls = [line.rstrip('\n') for line in open("app/static/urls-" + sid + ".txt")]

    actual_total = len(urls)

    extracted_data = extract_data_from_urls(urls)

    with open('app/static/nodes-' + sid + '.json') as f:
        nodes = json.load(f)

    # Build tree_data from urls and nodes.json data
    tree_data = build_outline_tree(urls,nodes)

    # Get a list of golden text html
    golden_pages = classifier.classify(extracted_data, urls)

    # Save the golden text to the huge HTML file.
    save_golden_pages(golden_pages, urls, sid)

    # Create full json file for save/load by user
    save_full_file(par_url,par_depth,actual_total,urls,golden_pages,tree_data,nodes,sid)

    resp = make_response(render_template('result.html', url=par_url, depth=par_depth,
                                        total=actual_total, urls=urls, data=golden_pages,
                                        tree_data=json.dumps(tree_data), nodes_data=json.dumps(nodes),
                                        sid=sid))

    # To Disable Caching on the web browser
    resp.headers.set('Cache-Control', "no-cache, no-store, must-revalidate")
    resp.headers.set('Pragma', "no-cache")
    resp.headers.set('Expires', "0")

    return resp

def save_golden_pages(golden_pages, urls, sid):
    with open('app/static/contents-' + sid +'.html', 'w') as f:
        f.write("<html>\n<head>\n<title>Article Contents Compilation</title>\n</head>\n<body>\n")
        for page, url in zip(golden_pages, urls):
            f.write("<div>\n")
            f.write("<h2>URL: {0}</h2>\n".format(url))
            f.write("{0}\n".format(page))
            f.write("</div>")
            f.write("<hr>\n")
        f.write("</body>\n</html>")

def save_full_file(par_url,par_depth,actual_total,urls,golden_pages,tree_data,nodes,sid):
    data = {}
    data['sid'] = sid
    data['url'] = par_url
    data['depth'] = par_depth
    data['total'] = actual_total
    data['urls'] = urls
    data['golden_pages'] = golden_pages
    data['tree_data'] = tree_data
    data['nodes'] = nodes

    with open('app/static/results-' + sid + '.wce', 'w') as fp:
            json.dump(data, fp, indent=4)

# Crawl and vizualize only form
@app.route('/crawl')
def crawl():
    return render_template('crawl.html')

# Result from Crawl and Visualize form
@app.route('/crawlresult')
def crawlresult():
    par_url = request.args.get('url')
    par_depth = request.args.get('depth')
    par_total = request.args.get('total')
    par_domain_specific = request.args.get('specific')

    # Generate a session ID for file name identification
    sid_length = 10
    sid = ''.join(secrets.choice(string.ascii_uppercase + string.digits)
                                                  for i in range(sid_length))

    run_crawler(par_url, par_depth, par_total, par_domain_specific, sid)

    urls = [line.rstrip('\n') for line in open("app/static/urls-" + sid + ".txt")]

    actual_total = len(urls)

    with open('app/static/nodes-' + sid + '.json') as f:
        nodes = json.load(f)

    # Build tree_data from urls and nodes.json data
    tree_data = build_outline_tree(urls, nodes)

    save_crawl_file(par_url,par_depth,actual_total,urls,tree_data,nodes,sid)

    resp = make_response(render_template('crawlresult.html', url=par_url, depth=par_depth,
                                        total=actual_total, urls=urls, tree_data=json.dumps(tree_data),
                                        nodes_data=json.dumps(nodes), sid=sid))

    # To Disable Caching on the web browser
    resp.headers.set('Cache-Control', "no-cache, no-store, must-revalidate")
    resp.headers.set('Pragma', "no-cache")
    resp.headers.set('Expires', "0")

    return resp

def save_crawl_file(par_url,par_depth,actual_total,urls,tree_data,nodes,sid):
    data = {}
    data['sid'] = sid
    data['url'] = par_url
    data['depth'] = par_depth
    data['total'] = actual_total
    data['urls'] = urls
    data['tree_data'] = tree_data
    data['nodes'] = nodes

    with open('app/static/results-' + sid + '.wce', 'w') as fp:
            json.dump(data, fp, indent=4)


# Web Extraction Form
@app.route('/extract')
def extract():
    return render_template('extract.html')

# Result from Web Extraction
@app.route('/extractresult', methods = ['POST'])
def extractresult():

    # Generate a session ID for file name identification
    sid_length = 10
    sid = ''.join(secrets.choice(string.ascii_uppercase + string.digits)
                                                  for i in range(sid_length))

    par_urls = request.form['urls'].split("\n")

    urls = [line.rstrip('\r') for line in par_urls]

    session['urls'] = urls
    # with open('app/static/urls.txt', 'w') as f:
    #             for url in urls:
    #                 f.write("%s\n" % url)

    actual_total = len(urls)

    extracted_data = extract_data_from_urls(urls)

    # Get a list of golden text html
    golden_pages = classifier.classify(extracted_data, urls)

    # Save the golden text to the huge HTML file.
    save_golden_pages(golden_pages, urls, sid)

    save_extract_file(golden_pages, urls, sid)

    resp = make_response(render_template('extractresult.html', total=actual_total, urls=urls,
                                        data=golden_pages, sid=sid))

    # To Disable Caching on the web browser
    resp.headers.set('Cache-Control', "no-cache, no-store, must-revalidate")
    resp.headers.set('Pragma', "no-cache")
    resp.headers.set('Expires', "0")

    return resp

def save_extract_file(golden_pages,urls,sid):
    data = {}
    data['sid'] = sid
    data['urls'] = urls
    data['golden_pages'] = golden_pages

    with open('app/static/results-' + sid + '.wce', 'w') as fp:
            json.dump(data, fp, indent=4)


def build_outline_tree(urls, nodes):
    # Build tree_data from urls and nodes.json data
    tree_data = []

    for idx, url in enumerate(urls):
        # The first url is always the root with no parents
        # index start with 1
        if idx == 0:
            node = {
                "id": idx+1,
                "parent": "#",
                "text": "".join([str(idx+1), ". ", url]),
                "icon": "jstree-file"
            }
            tree_data.append(node)
        else:
            # Find the parent link of the current url, then find the index of that parent link + 1
            parent_link = ""
            for link in nodes['links']:
                if link['target'] == url:
                    parent_link = link['source']
                    parent_idx = urls.index(parent_link) + 1
            if parent_link == "":
                parent_idx = "#"
                print(url)

            node = {
                "id": idx+1,
                "parent": parent_idx,
                "text": "".join([str(idx+1), ". ", url]),
                "icon": "jstree-file",
                "li_attr": {
                    "class": "url"
                }
            }
            tree_data.append(node)
    return tree_data

# load file form
@app.route('/load')
def load():
    return render_template('load.html')

# Result according to file upload
@app.route('/loadresult', methods = ['POST'])
def loadresult():
    data = {}

    if request.method == 'POST':
        f = request.files['file']
        data = json.load(f)
    else:
        return "Error: POST method is not used"
    # par_url,par_depth,actual_total,urls,golden_pages,tree_data,nodes,sid
    # Check if it is full results of both crawl and extract
    if "url" in data and "golden_pages" in data:
        sid = data['sid']
        par_url = data['url']
        par_depth = data['depth']
        actual_total = data['total']
        urls = data['urls']
        golden_pages = data['golden_pages']
        tree_data = data['tree_data']
        nodes = data['nodes']

        with open('app/static/nodes-' + sid + '.json', 'w') as fp:
            json.dump(nodes, fp, indent=4)

        with open('app/static/urls-'+ sid + '.txt', 'w') as f:
            for url in urls:
                f.write("%s\n" % url)

        with open('app/static/results-' + sid + '.wce', 'w') as fp:
            json.dump(data, fp, indent=4)

        # Save the golden text to the huge HTML file.
        save_golden_pages(golden_pages, urls, sid)

        resp = make_response(render_template('result.html', url=par_url, depth=par_depth,
                                        total=actual_total, urls=urls, data=golden_pages,
                                        tree_data=json.dumps(tree_data), nodes_data=json.dumps(nodes),
                                        sid=sid))

        # To Disable Caching on the web browser
        resp.headers.set('Cache-Control', "no-cache, no-store, must-revalidate")
        resp.headers.set('Pragma', "no-cache")
        resp.headers.set('Expires', "0")

        return resp

    # Check if it is crawl result only
    elif "url" in data:
        sid = data['sid']
        par_url = data['url']
        par_depth = data['depth']
        actual_total = data['total']
        urls = data['urls']
        tree_data = data['tree_data']
        nodes = data['nodes']

        with open('app/static/nodes-' + sid + '.json', 'w') as fp:
            json.dump(nodes, fp, indent=4)

        with open('app/static/urls-'+ sid + '.txt', 'w') as f:
            for url in urls:
                f.write("%s\n" % url)

        with open('app/static/results-' + sid + '.wce', 'w') as fp:
            json.dump(data, fp, indent=4)


        resp = make_response(render_template('crawlresult.html', url=par_url, depth=par_depth,
                                        total=actual_total, urls=urls, tree_data=json.dumps(tree_data),
                                        nodes_data=json.dumps(nodes), sid=sid))

        # To Disable Caching on the web browser
        resp.headers.set('Cache-Control', "no-cache, no-store, must-revalidate")
        resp.headers.set('Pragma', "no-cache")
        resp.headers.set('Expires', "0")

        return resp
    # Check if it is extract result only
    elif "golden_pages" in data:
        sid = data['sid']
        urls = data['urls']
        golden_pages = data['golden_pages']

        # Save the golden text to the huge HTML file.
        save_golden_pages(golden_pages, urls, sid)

        resp = make_response(render_template('extractresult.html', urls=urls,
                                            data=golden_pages, sid=sid))

        # To Disable Caching on the web browser
        resp.headers.set('Cache-Control', "no-cache, no-store, must-revalidate")
        resp.headers.set('Pragma', "no-cache")
        resp.headers.set('Expires', "0")

        return resp
    else:
        return "Uploaded is invalid"
