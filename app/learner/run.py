#!/usr/bin/env python

import os
import sys

lib_path = os.path.realpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', 'lib'))
if lib_path not in sys.path:
    sys.path[0:0] = [lib_path]

import customutils
import clusterers
import processors
import tokenizers
import analyzers
import simplejson as json
import math
import os
import argparse

def main(args):

    path = customutils.get_data_path(args.site[0])
    urls = customutils.load_urls(path)

    print('[learner] clustering with %d urls' % len(urls))

    # # Label all text blocks as zero.
    # [customutils.label_texts(path, str(id).zfill(get_total_digit(len(urls)))) for id, url in enumerate(urls)]

    # load data
    data = [customutils.load_data(path, str(id).zfill(get_total_digit(len(urls)))) for id, url in enumerate(urls)]

    # process data
    processor = processors.Processor(data, tokenizer=tokenizers.GenericTokenizer, analyzer=analyzers.LongestAnalyzer)
    features = processor.extract()

    # clustering
    clusterer = clusterers.DBSCAN()
    labels = clusterer.cluster(features).labels_

    # score
    clusters = processor.score(labels)

    # sort by highest score
    sorted_clusters = sorted(clusters, key=lambda cluster: cluster['score'], reverse=True)

    # save to a file
    with open(os.path.join(path, 'clusters.json'), 'w') as f:
        f.write(json.dumps(list(sorted_clusters), indent=2, ensure_ascii=False))

def get_total_digit(num):
    return int(math.log10(num))+1

def parse_args():
    """
    Parse commandline arguments
    """
    parser = argparse.ArgumentParser(description='Run the whole pipeline on site pages.')
    parser.add_argument('site', metavar='site', type=str, nargs=1, help='site id, for example: theverge, npr, nytimes')
    return parser.parse_args()

if __name__ == '__main__':
    main(parse_args())
