#!/usr/bin/env python

import os
import sys

lib_path = os.path.realpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', 'lib'))
if lib_path not in sys.path:
    sys.path[0:0] = [lib_path]

import customutils
import clusterers
import processors
import simplejson as json
import math
import os
import argparse
import numpy as np
from sklearn.feature_extraction import FeatureHasher
from sklearn import preprocessing, model_selection
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.utils import class_weight
import collections
import random
import time
import psutil
import pickle
from tensorflow.keras.models import Sequential, load_model, model_from_json
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from matplotlib import pyplot

def main():
    start_time = time.process_time()
    MULTICLASS = True
    TRAIN = True

    # For site specific Training
    # sites = ['detik', 'iwanbanaran', 'kompas', 'teknojurnal', 'tipspintar']
    # data = []
    # for site in sites:

    #     path = customutils.get_data_path(site)
    #     urls = customutils.load_urls(path)

    #     # load data
    #     data = data + [customutils.load_data(path, str(id).zfill(get_total_digit(len(urls)))) for id, url in enumerate(urls)]

    if TRAIN:
        if MULTICLASS:
            # load all the accumulated json data from detik,iwanbanaran, etc.
            with open('app/static/full_multiclass.json') as f:
                data = json.load(f)
        else:
            # load all the accumulated json data from detik,iwanbanaran, etc.
            with open('app/static/full_binclass.json') as f:
                data = json.load(f)
    else:
        # load data acak for testing NN only
        data = []
        if MULTICLASS:
            path = customutils.get_data_path("acak_multiclass")
        else:
            path = customutils.get_data_path("acak_binclass")
        urls = customutils.load_urls(path)
        data = data + [customutils.load_data(path, str(id).zfill(2)) for id, url in enumerate(urls)]

    # process data
    processor = processors.Processor(data)

    # release data from memory
    del data

    # prepare features
    discrete_features, labels = processor.prepare()

    if MULTICLASS:
        binarizer = preprocessing.LabelBinarizer()
        labels = binarizer.fit_transform(labels)
    else:
        binarizer = None

    NUM_FEATURES = 188
    vectorizer = FeatureHasher(n_features=NUM_FEATURES)
    discrete_features = vectorizer.fit_transform(discrete_features).toarray()

    # Set np print output to max so it shows all data.
    np.set_printoptions(threshold=sys.maxsize)

    # print("Example of Labels: ")
    # print(labels[1700])

    # save processor object to a file for future use
    # with open('app/static/processor.pkl', 'wb') as f:
    #     pickle.dump(processor, f)

    # release processor object from memory
    del processor

    # Split dataset when it is only for one website
    # labels = np.array(labels)
    # train_x, test_x, train_y, test_y = model_selection.train_test_split(discrete_features, labels, test_size=0.25, random_state=42)

    if TRAIN:
        # Split for combined dataset of the 5 websites.
        train_x, test_x, train_y, test_y = split_dataset(discrete_features, labels)
    else:
        # For testing NN only
        test_x = discrete_features
        test_y = np.array(labels)

    del discrete_features
    del labels

    # test_x = np.genfromtxt("test_x.csv", delimiter=",")
    # test_y = np.genfromtxt("test_y.csv", delimiter=",")

    # np.savetxt("train_x.csv", train_x, delimiter=",")
    # np.savetxt("test_x.csv", test_x, delimiter=",")
    # np.savetxt("train_y.csv", train_y, delimiter=",")
    # np.savetxt("test_y.csv", test_y, delimiter=",")

    # Cut the amount for Dataset to articles of detik.com for quick testing purpose
    # train_x, test_x, train_y, test_y = train_x[1635:3617], test_x[1635:2100], train_y[1635:3617], test_y[1635:2100]

    if MULTICLASS:
        param = {
            'out_dim': 4,
            'out_act': 'softmax',
            'loss_func': 'categorical_crossentropy'
        }
    else:
        param = {
            'out_dim': 1,
            'out_act': 'sigmoid',
            'loss_func': 'binary_crossentropy'
        }


    # Initialized an NN model, train, and save the best model to a file
    if TRAIN:
        train_nn(NUM_FEATURES, train_x, train_y, test_x, test_y, param)

    # load the NN model from a file and test the dataset
    test_nn(test_x, test_y, param, binarizer)

    end_time = time.process_time()
    print("Time elapsed: ", end_time - start_time)

    # check memory usage
    process = psutil.Process(os.getpid())
    print(process.memory_info())

def train_nn(NUM_FEATURES, train_x, train_y, test_x, test_y, param):

    # Initializing Neural Network
    model = Sequential()

    # Defining the layers, neurons, and activation function
    model.add(Dense(units = 96, activation = 'relu', input_dim = NUM_FEATURES))
    model.add(Dropout(0.2))
    model.add(Dense(units = 20, activation = 'relu'))
    model.add(Dropout(0.2))
    model.add(Dense(units = param['out_dim'], activation = param['out_act']))

    # Defining the optimizer and loss function
    model.compile(optimizer = 'adam', loss = param['loss_func'], metrics=['accuracy'])

    # Set early stopping and best model checkpoint
    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10)
    mc = ModelCheckpoint('best_model.h5', monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)

    # Set class weight as dataset is extremely imbalanced
    # if param['out_dim'] == 4:
    #     ints_y = [y.argmax() for y in train_y]
    #     class_weights = class_weight.compute_class_weight('balanced',
    #                                                 np.unique(ints_y),
    #                                                 ints_y)
    # else:
    #     class_weights = class_weight.compute_class_weight('balanced',
    #                                                 np.unique(train_y),
    #                                                 train_y)
    # print(class_weights)

    # Training the model
    history = model.fit(train_x, train_y, validation_data=(test_x, test_y), batch_size = 1, epochs = 100, verbose=0, callbacks=[es, mc])


    model = load_model('best_model.h5')

    # Evaluate the model
    _, train_acc = model.evaluate(train_x, train_y, verbose=0)
    _, test_acc = model.evaluate(test_x, test_y, verbose=0)
    print("Training Neural Network 2 hidden layers consisting of 96 and 20 neurons (with a dropout of 0.5 in between). Features are tag path and css properties")
    print('Train: %.3f, Test: %.3f' % (train_acc, test_acc))

    # plot training history
    plot_training_history(history)

    # save the model to disk
    save_nn_model(model)

def plot_training_history(history):
    # plot training history
    # plot loss during training
    pyplot.subplot(211)
    pyplot.title('loss')
    pyplot.plot(history.history['loss'], label='train')
    pyplot.plot(history.history['val_loss'], label='test')
    pyplot.legend()
    # plot accuracy during training
    pyplot.subplot(212)
    pyplot.title('Accuracy')
    pyplot.plot(history.history['accuracy'], label='train')
    pyplot.plot(history.history['val_accuracy'], label='test')
    pyplot.legend()
    pyplot.show()

def save_nn_model(model):
    # serialize model to JSON
    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model.h5")


def test_nn(test_x, test_y, param, binarizer=None):
    # load json and create model
    model = load_nn_model()

    # Defining the optimizer and loss function
    model.compile(optimizer = 'adam', loss = param['loss_func'], metrics=['accuracy'])

    # print('Neural Network Model Summary: ')
    print(model.summary())

    # Evaluate the model
    _, test_acc = model.evaluate(test_x, test_y, verbose=0)
    print('Accuracy: %.3f%%' % (test_acc*100))


    # Predict using the model
    pred_y = model.predict(test_x)

    if binarizer is None:
        # for binary classification
        pred_y = [1 if y>=0.5 else 0 for y in pred_y]
    else:
        # for multiclass classification
        # convert softmax output values to either one or zero
        for i in range(len(pred_y)):
            for j in range(4):
                pred_y[i][j] = 1 if np.argmax(pred_y[i]) == j else 0
        # Change the values data type from float to int
        pred_y = pred_y.astype(int)


    # Calculating the test result
    accuracy = accuracy_score(test_y, pred_y)

    if binarizer is None:
        precision = precision_score(test_y, pred_y)
        recall = recall_score(test_y, pred_y)
        f1 = f1_score(test_y, pred_y)
    else:
        precision = precision_score(test_y, pred_y, average=None, labels=[0,1,2,3], zero_division=0)
        recall = recall_score(test_y, pred_y, average=None, labels=[0,1,2,3], zero_division=0)
        f1 = f1_score(test_y, pred_y, average=None, labels=[0,1,2,3], zero_division=0)
        pred_y = binarizer.inverse_transform(pred_y)

    # accuracy: (tp + tn) / (p + n)
    print('Accuracy: %.3f%%' % (accuracy*100))
    # precision tp / (tp + fp)
    print('Precision:')
    print(precision)
    # recall: tp / (tp + fn)
    print('Recall:')
    print(recall)
    # f1: 2 tp / (2 tp + fp + fn)
    print('F1 score:')
    print(f1)

    # Save Prediction
    np.savetxt("pred_y.csv", pred_y[:100], delimiter=",")

def load_nn_model():
    json_file = open('model.json', 'r')
    model_json = json_file.read()
    json_file.close()
    model = model_from_json(model_json)
    # load weights into new model
    model.load_weights("model.h5")
    return model

def split_dataset(discrete_features, labels):
    IWANBANARAN_IDX = 4820
    KOMPAS_IDX = 14745
    TEKNOJURNAL_IDX = 25061
    TIPSPINTAR_IDX = 27147

    # Split the dataset by websites
    detik_x, iwanbanaran_x, kompas_x, teknojurnal_x, tipspintar_x = np.split(discrete_features, [IWANBANARAN_IDX, KOMPAS_IDX, TEKNOJURNAL_IDX, TIPSPINTAR_IDX])
    detik_y, iwanbanaran_y, kompas_y, teknojurnal_y, tipspintar_y = np.split(labels, [IWANBANARAN_IDX, KOMPAS_IDX, TEKNOJURNAL_IDX, TIPSPINTAR_IDX])

    # Split each website dataset to train and test dataset
    detik_train_x, detik_test_x, detik_train_y, detik_test_y = model_selection.train_test_split(detik_x, detik_y, test_size=0.25, random_state=42)
    iwanbanaran_train_x, iwanbanaran_test_x, iwanbanaran_train_y, iwanbanaran_test_y = model_selection.train_test_split(iwanbanaran_x, iwanbanaran_y, test_size=0.25, random_state=42)
    kompas_train_x, kompas_test_x, kompas_train_y, kompas_test_y = model_selection.train_test_split(kompas_x, kompas_y, test_size=0.25, random_state=42)
    teknojurnal_train_x, teknojurnal_test_x, teknojurnal_train_y, teknojurnal_test_y = model_selection.train_test_split(teknojurnal_x, teknojurnal_y, test_size=0.25, random_state=42)
    tipspintar_train_x, tipspintar_test_x, tipspintar_train_y, tipspintar_test_y = model_selection.train_test_split(tipspintar_x, tipspintar_y, test_size=0.25, random_state=42)

    # Combine the datasets back into one big dataset
    train_x = np.concatenate([detik_train_x, iwanbanaran_train_x, kompas_train_x, teknojurnal_train_x, tipspintar_train_x])
    test_x = np.concatenate([detik_test_x, iwanbanaran_test_x, kompas_test_x, teknojurnal_test_x, tipspintar_test_x])
    train_y = np.concatenate([detik_train_y, iwanbanaran_train_y, kompas_train_y, teknojurnal_train_y, tipspintar_train_y])
    test_y = np.concatenate([detik_test_y, iwanbanaran_test_y, kompas_test_y, teknojurnal_test_y, tipspintar_test_y])

    return train_x, test_x, train_y, test_y

def get_total_digit(num):
    return int(math.log10(num))+1

if __name__ == '__main__':
    main()
