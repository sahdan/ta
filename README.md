# Web Content Extractor
This is a Python based project that is done as the final project to finish undergraduate study.
This project is a web application that run using Flask, Scrapy, Splash, and TensorFlow.
It also need Splash Docker image to be run on the same host as the web server.
This project was developed and tested only on Linux Ubuntu 18.04.3 LTS

## What is this project about?
This project enable you to automate the process of crawling the internet, get the visualization data in the proccess, and get the article-like contents in the crawled web pages if any.

The Neural Network model used for this project is trained only using 5 websites:
* detik.com
* iwanbanaran.com
* kompas.com
* teknojurnal.com
* tipspintar.com

To understand the gist of this project like its architecture and its reports. See the Google Slides presentation here:
https://drive.google.com/file/d/17uyBfPy7Yty9M76LQxYeP-7nokLKxQVG/view?usp=sharing

## Requirements
1. Install Docker and Splash. https://splash.readthedocs.io/en/stable/install.html
2. Python Virtual Environment venv
3. All libraries in Requirements.txt are installed in the virtual environment.
4. The virtual environment has been activated.

## How to run
1. From the Bash Terminal, run the Flask Web Server from the project root folder.:
`flask run --host=0.0.0.0`
2. From the Bash Terminal, run the Splash Docker image (sudo might be needed):
`docker run -it -p 8050:8050 --rm scrapinghub/splash`
3. Open your browser, and go to http://localhost:5000/. Fill the form and let it do its magic. Please keep in mind that the process takes a while to finish depending on the number of web pages you requested.

Please keep in mind that as of this README file is written, there is no plan to develop this project further.
The repository is uploaded as is.
This project is NOT production ready by any means.
